﻿using UnityEngine;
using System.Collections;

public class SpawnZone : MonoBehaviour
{
    public string Type = "Enemy";
    [Range(0, 10000)]
    public float SpawnRate = 100.0f;
    private float last = 0;
    private BoxCollider zone;
    private GameObject enemyPrefab;
    private GameObject player;
    // Use this for initialization
    void Start()
    {
        zone = GetComponent<BoxCollider>();
        enemyPrefab = Resources.Load("Prefabs/" + Type) as GameObject;
        SpawnRate /= 1000.0f;
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > last + SpawnRate && player != null)
        {
            Vector3 offset = new Vector3(
                zone.size.x * Random.Range(-0.5f, 0.5f),
                zone.size.y * Random.Range(-0.5f, 0.5f),
                zone.size.z * Random.Range(-0.5f, 0.5f));
            Instantiate(enemyPrefab, transform.position + offset, transform.rotation);

            last = Time.time;
        }
    }
}
