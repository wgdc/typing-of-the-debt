﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
    private string _text;
    public string Text
    {
        get { return _text; }
        set
        {
            _text = value;
            displayText.text = value;
        }
    }
    private TextMesh displayText;
    private TextMesh scoreText;
    private TextMesh livesText;
    private GameObject bullet;
    private GameObject mainCamera;
    private string gameOverText;
    // Use this for initialization
    private int score;
    private int lives;
    void Start()
    {
        displayText = GameObject.Find("Input").GetComponent<TextMesh>();
        scoreText = GameObject.Find("Score").GetComponent<TextMesh>();
        livesText = GameObject.Find("Lives").GetComponent<TextMesh>();
        lives = 3;
        livesText.text = "Lives: " + lives;
        score = 0;
        scoreText.text = "Score: " + score;
        bullet = Resources.Load("Prefabs/WordBullet") as GameObject;
        mainCamera = GameObject.Find("Main Camera");
        mainCamera.SetActive(false);
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Backspace) && Text.Length > 0)
        {
            Text = Text.Remove(Text.Length - 1);
        }
        else if (Input.GetKeyDown(KeyCode.Return) && Text.Length > 0)
        {
            shoot();
        }
        else if (Input.anyKeyDown)
        {
            Text += Input.inputString;
        }

        foreach (Enemy enemy in Enemy.Enemies)
        {
            bool died = enemy.Check(Text, true);
        }
    }

    private void shoot()
    {
        foreach (Enemy enemy in Enemy.Enemies)
        {
            bool died = enemy.Check(Text);
            if (!died) continue;
            (Instantiate(bullet, displayText.transform.position, transform.rotation) as GameObject).GetComponent<WordBullet>().GO(enemy);

            Text = "";
            score++;
            scoreText.text = "Score: " + score;
            break;
        }
    }

    void OnCollisionEnter(Collision col)
    {
        Enemy enemy = col.gameObject.GetComponent<Enemy>();
        if (enemy != null)
        {
            lives--;
            enemy.GetKilled();
            livesText.text = "Lives: " + lives;
            if (lives == 0)
            {
                GameOver();
            }
        }
    }

    void GameOver()
    {
        Text = "Game Over";
        mainCamera.SetActive(true);
        GetComponentInChildren<Camera>().enabled = false;
        StartCoroutine(WaitAndRestart());
    }

    private IEnumerator WaitAndRestart()
    {
        yield return new WaitForSeconds(3);
        Application.LoadLevel(Application.loadedLevelName);
    }
}
