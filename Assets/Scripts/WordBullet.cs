﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WordBullet : MonoBehaviour
{ 
    private Vector3 Target;
    private Enemy enemy;
	// Use this for initialization
	void Start ()
	{
	    
    }
	
	// Update is called once per frame
	void Update ()
	{
	    transform.position = Vector3.Lerp(transform.position, Target, 0.5f);
	    if (Vector3.Distance(transform.position, Target) < 0.1f)
	    {
	        enemy.GetKilled();
            Destroy(gameObject);
	    }
	}

    public void GO(Enemy inEnemy)
    {
        Target = inEnemy.GetWordsPosition();
        GetComponent<TextMesh>().text = inEnemy.Text;
        enemy = inEnemy;
    }

}
