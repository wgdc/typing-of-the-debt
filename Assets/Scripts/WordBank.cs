﻿using UnityEngine;
using System.Collections.Generic;

public static class WordBank {

    const int EASY_LENGTH_LIMIT = 6;
    const int MEDIUM_LENGTH_LIMIT = 10;

    static List<string> easyWords = new List<string>();
    static List<string> medWords = new List<string>();
    static List<string> hardWords = new List<string>();

    static WordBank()
    {
        string[] files = { "Assets/Resources/random_words.txt" };
        string[] words;

        foreach (string file in files)
        {
            string contents = System.IO.File.ReadAllText(file);
            if (contents != null)
            {
                words = contents.Split(',');
                foreach (string word in words)
                {
                    if (word.Length <= EASY_LENGTH_LIMIT)
                    {
                        if (!easyWords.Contains(word))
                            easyWords.Add(word);
                    }
                    else if (word.Length <= MEDIUM_LENGTH_LIMIT)
                    {
                        if (!medWords.Contains(word))
                            medWords.Add(word);
                    }
                    else if (!hardWords.Contains(word))
                    {
                        hardWords.Add(word);
                    }
                }
            }
        }
    }

    public static string GetEasyWord()
    {
        return easyWords[Random.Range(0, easyWords.Count)];
    }

    public static string GetMediumWord()
    {
        return medWords[Random.Range(0, medWords.Count)];
    }

    public static string GetHardWord()
    {
        return hardWords[Random.Range(0, hardWords.Count)];
    }
}
